package io.spring.batch.component;

public class CustomRetryableException extends Exception  {
	
	public CustomRetryableException() {
		super();
	}

	public CustomRetryableException(String msg) {
		super(msg);
	}
}
